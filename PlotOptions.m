classdef PlotOptions
    % Plotting options that collectively apply to the blocks included in
    % HeatmapCovariatePlot.
    
    properties
        % Font name for text;
        font_name;
        
        % Font size for text;
        font_size;
        
        % Margin to make space for subblock title, in mm;
        subblock_title_margin;
        
        % Space for left labels, in mm;
        left_labels_space;
        
        % Height of each covariate row, in mm. Also used for covariate,
        % cluster annotations and colorbar widths;
        covariate_row_height;
        
        % Distance between covariate rows, in mm;
        covariate_row_margin;
        
        % Space between covariate block and corresponding legend, in mm;
        covariate_legend_intraspace;
        
        % Space between legend entries, in mm;
        covariate_legend_entry_width;
        
        % Space between legend box and text, in mm;
        covariate_legend_text_margin;
        
        % Distance between elements inside the heatmap block, in mm;
        heatmap_margin;
        
        % Top dendrogram height / right dendrogram width, in mm;
        heatmap_dendrogram_size;
        
        % Font size for heatmap labels;
        heatmap_labels_font_size;
        
        % Draw vertical lines separating samples
        enable_sample_lines;
        
        % Order to plot samples (rows of data), array with a permutation of 1:n;
        sample_order;
        
        % Maximum number of entries for metadata legends;
        n_legend_entries;
        
        % Space for right legend panels, in mm;
        legend_block_width;
    end
    
end
