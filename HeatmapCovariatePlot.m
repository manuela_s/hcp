classdef HeatmapCovariatePlot < handle
    % Class to create heatmap plot with multiple covariates.
    % Example use:
    % data = array2table(randn(100, 12));
    % [~, ~, bins] = histcounts(data.Var2, 3);
    % data.Var2 = categorical(bins);
    % p = HCP.HeatmapCovariatePlot(data);
    % p.blocks.AddCovariateRow('Var1', 'title', 'Metadata 1', 'cmap', jet());
    % p.blocks.AddCovariateRow('Var2', 'title', 'Metadata 2');
    % p.blocks.AddHeatmap(data.Properties.VariableNames(3:width(data)));
    % p.Plot();
    %
    % See also HCP.BlockContainer.
    
    properties (Access = private)
        % Table with data for the plot;
        data;
        % Parsing results from input parser;
        ip_results;
        % Handle for panel we plotted into;
        panel;
    end
    
    properties
        % Instance of BlockContainer;
        % See also HCP.BlockContainer.
        blocks;
    end
    
    methods
        function obj = HeatmapCovariatePlot(data, varargin)
            % Constructor for 'HCP.HeatmapCovariatePlot'.
            % Input argument:
            % - data: table with data to plot;
            % - key/value pairs:
            %   - font_name: string, name for font. Defaults to 'arial';
            %   - font_size: scalar, size for font. Defaults to 8;
            %   - subblock_title_margin: scalar, margin for subblock
            %     titles, in mm. Defaults to 10;
            %   - left_labels_space: scalar, space for left labels, in mm.
            %     Defaults to 35;
            %   - covariate_row_height: scalar, height of each covariate row,
            %     in mm. Also used for covariate cluster annotations and
            %     colorbar widths. Defaults to 2;
            %   - covariate_row_margin: scalar, distance between covariate
            %     rows, in mm. Defaults to 2;
            %   - covariate_legend_intraspace: scalar, space between covariate
            %     block and corresponding legend, in mm. Defaults to 5;
            %   - covariate_legend_entry_width: space between legend entries,
            %     in mm. Defaults to 15;
            %   - covariate_legend_text_margin: space between legend box and
            %     text, in mm. Defaults to 1;
            %   - heatmap_margin: distance between elements inside the heatmap
            %     block, in mm. Defaults to 1.5;
            %   - heatmap_dendrogram_size: scalar, top dendrogram height /
            %     right dendrogram width, in mm. Defaults to 15;
            %   - heatmap_labels_font_size: scalar, font size for heatmap
            %     labels. Defaults to 6.
            
            assert(istable(data));
            obj.data = data;
            obj.blocks = HCP.BlockContainer(data);
            
            ip = inputParser();
            ip.addParameter('font_name', 'arial', @ischar);
            ip.addParameter('font_size', 8, @isnumeric);
            ip.addParameter('subblock_title_margin', 10, @isnumeric);
            ip.addParameter('left_labels_space', 35, @isnumeric);
            ip.addParameter('covariate_row_height', 2, @isnumeric);
            ip.addParameter('covariate_row_margin', 2, @isnumeric);
            ip.addParameter('covariate_legend_intraspace', 5, @isnumeric);
            ip.addParameter('covariate_legend_entry_width', 15, @isnumeric);
            ip.addParameter('covariate_legend_text_margin', 1, @isnumeric);
            ip.addParameter('heatmap_margin', 1.5, @isnumeric);
            ip.addParameter('heatmap_dendrogram_size', 15, @isnumeric);
            ip.addParameter('heatmap_labels_font_size', 6, @isnumeric);
            ip.addParameter('enable_sample_lines', true, @islogical);
            ip.parse(varargin{:});
            
            obj.ip_results = ip.Results;
        end
        
        function options = BuildPlotOptions(obj)
            % Create PlotOptions.
            % Returns:
            % - options: instance of PlotOptions.
            
            options = HCP.PlotOptions();
            
            options.font_name = obj.ip_results.font_name;
            options.font_size = obj.ip_results.font_size;
            options.subblock_title_margin = obj.ip_results.subblock_title_margin;
            options.left_labels_space = obj.ip_results.left_labels_space;
            options.covariate_row_height = obj.ip_results.covariate_row_height;
            options.covariate_row_margin = obj.ip_results.covariate_row_margin;
            options.covariate_legend_intraspace = obj.ip_results.covariate_legend_intraspace;
            options.covariate_legend_entry_width = obj.ip_results.covariate_legend_entry_width;
            options.covariate_legend_text_margin = obj.ip_results.covariate_legend_text_margin;
            options.heatmap_margin = obj.ip_results.heatmap_margin;
            options.heatmap_dendrogram_size = obj.ip_results.heatmap_dendrogram_size;
            options.heatmap_labels_font_size = obj.ip_results.heatmap_labels_font_size;
            options.enable_sample_lines = obj.ip_results.enable_sample_lines;
            
            options.sample_order = obj.blocks.GetSampleOrder();
            if isempty(options.sample_order)
                options.sample_order = 1:height(obj.data);
            end
            options.n_legend_entries = obj.blocks.GetNLegendEntries();
            options.legend_block_width = options.covariate_legend_intraspace +...
                (options.covariate_row_height + options.covariate_legend_entry_width)*...
                options.n_legend_entries;
        end
        
        function Plot(obj, varargin)
            % Plot HeatmapCovariatePlot with all the blocks.
            % Input argument:
            % - key/value pair:
            %   - parent_panel: instance of panel class, panel to plot
            %     into. Defaults to create a new panel in the current figure.
            
            ip = inputParser();
            ip.addParameter('parent_panel', []);
            ip.parse(varargin{:});
            if isempty(ip.Results.parent_panel)
                obj.panel = panel('defer');
            else
                obj.panel = ip.Results.parent_panel;
            end
            
            options = obj.BuildPlotOptions();
            
            obj.panel.margin = 2;
            obj.panel.marginleft = options.subblock_title_margin;
            obj.blocks.Plot(obj.panel, options);
            obj.panel.fontsize = options.font_size;
            obj.panel.fontname = options.font_name;
            obj.panel.refresh();
        end
        
        function Export(obj, filename, varargin)
            % Export HeatmapCovariatePlot figure to a graphic file.
            % Input arguments:
            % - filename: string, name of the file to create. File
            %   extension is used to determine file format. Supports pdf
            %   and other formats supported by panel.export.
            % - key/value pairs are passed on to panel.export.
            %   See also panel.export.
            
            if isempty(obj.panel)
                obj.Plot();
            end
            
            options = obj.BuildPlotOptions();
            height = obj.blocks.GetBlockHeight(options) + 2*2;
            obj.panel.export(filename, sprintf('-h%d', height), varargin{:});
        end
    end
    
end
