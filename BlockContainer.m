classdef BlockContainer < HCP.Block
    % Container for a group of blocks for 'HeatmapCovariatePlot'.
    % 'HeatmapCovariatePlot' has a top level 'BlockContainer'. The
    % 'BlockContainer' can hold leaf blocks like CovariateRow or Heatmap
    % or other 'BlockContainer's (sub-blocks).
    
    properties
        % String, title for block rendered on the left side of the block;
        title;
        
        % Cell array, instances of 'Block's held by the 'BlockContainer';
        sub_blocks = {};
    end
    
    methods
        function obj = BlockContainer(data, varargin)
            % Constructor for 'HCP.BlockContainer'.
            % Input arguments:
            % - data: MATLAB table with data to plot;
            % - key/value pair:
            %   - title: string, title for the block. Defaults to no title.
            
            ip = inputParser();
            ip.addParameter('title', '', @ischar);
            ip.parse(varargin{:});
            
            obj = obj@HCP.Block(data);
            obj.title = ip.Results.title;
        end
        
        function block = AddSubBlock(obj, varargin)
            % Append sub-block (a new 'BlockContainer') to the current
            % block.
            % Input arguments are passed to 'BlockContainer' constructor.
            % See also HCP.BlockContainer.
            
            block = HCP.BlockContainer(obj.data, varargin{:});
            obj.sub_blocks{end+1} = block;
        end
        
        function row = AddCovariateRow(obj, varargin)
            % Append 'CovariateRow' to the current block.
            % Input arguments are passed to 'CovariateRow' constructor.
            % See also HCP.CovariateRow.
            
            row =  HCP.CovariateRow(obj.data, varargin{:});
            obj.sub_blocks{end+1} = row;
        end
        
        function hm = AddHeatmap(obj, varargin)
            % Append 'Heatmap' to the current block.
            % Input arguments are passed to 'Heatmap' constructor.
            % See also HCP.Heatmap.
            
            hm =  HCP.Heatmap(obj.data, varargin{:});
            obj.sub_blocks{end+1} = hm;
        end
        
        function sample_order = GetSampleOrder(obj)
            
            sample_orders = cellfun(@(x) x.GetSampleOrder(), obj.sub_blocks,...
                'UniformOutput', false);
            
            % Ignore sub_blocks that do not specify a sample_order
            sample_orders(cellfun(@isempty, sample_orders)) = [];
            
            switch numel(sample_orders)
                case 0
                    % No block specifies a sample order
                    sample_order = [];
                case 1
                    sample_order = sample_orders{1};
                otherwise
                    error('HCP:GetSampleOrder:OnlyOneSampleOrderCanBeSpecified',...
                        'Only one set of sample order can be specified');
            end
        end
        
        function n_legend_entries = GetNLegendEntries(obj)
            
            n_legend_entries = max(...
                [cellfun(@(x) x.GetNLegendEntries(), obj.sub_blocks), 0]);
        end
        
        function height = GetBlockHeight(obj, options)
            
            height = sum(...
                cellfun(@(x) x.GetBlockHeight(options), obj.sub_blocks)) + ...
                options.covariate_row_margin * numel(obj.sub_blocks);
        end
        
        function Plot(obj, p, options)
            
            heights = cellfun(@(x) {x.GetBlockHeight(options)}, obj.sub_blocks,...
                'UniformOutput', false);
            
            p.pack('v', heights);
            p.children.margintop=options.covariate_row_margin;
            p.children.marginbottom=options.covariate_row_margin;
            
            for i = 1:numel(obj.sub_blocks)
                obj.sub_blocks{i}.Plot(p(i), options);
            end
            
            if ~isempty(obj.title)
                p.ylabel(obj.title);
            end
        end
    end
end
