classdef Block < handle
    % Building block for HeatmapCovariatePlot.
    % Each instance of this block represents an horizontal block of the
    % overall plot. This is an abstract class that is never used directly,
    % instead sub-classes (BlockContainer, CovariateRow and Heatmap) are
    % created by calling Add* methods in BlockContainer.
    
    properties
        % Table with data required for the block;
        data;
    end
    
    methods
        function obj = Block(data)
            % Constructor for 'HCP.Block'.
            % Input argument:
            % - data: table with data to plot.
            
            obj.data = data;
        end
        
        function [p_left, p_main, p_right] = HorizontalBlockSplit(~, p, options)
            % Create horizontal sub-panels for block.
            % Each block is split into 3 panels horizontally:
            % - a left panel for labelling features;
            % - a main panel for graphic;
            % - a right panel for annotating features such as legend,
            %   dendrogram for heatmaps, etc.
            % Input arguments:
            % - p: instance of panel class for the panel to be used for
            %   rendering the block;
            % - options: instance of PlotOptions class representing the plot
            %   options for HeatmapCovariatePlot;
            % Returns:
            % - p_left: left metadata panel;
            % - p_main: main panel;
            % - p_right: right metadata panel.
            
            p.pack('h',...
                {{options.left_labels_space}, [], {options.legend_block_width}});
            p_left = p(1);
            p_main = p(2);
            p_right = p(3);
            p_left.marginright = 0;
            p_main.marginleft = 0;
            p_main.marginright = 0;
            p_right.marginleft = 0;
        end
    end
    
    methods (Static)
        function AddSampleLines(n_samples, color, options)
            % Add vertical lines over the plot to separate samples.
            % Input arguments:
            % - n_samples: number of samples;
            % - color: color, either a color name or an array with RGB
            %   values, to use for separation lines;
            % - options: instance of PlotOptions class.
            
            if options.enable_sample_lines
                line(repmat(1.5:n_samples-0.5, 2, 1),...
                    repmat(get(gca, 'YLim')', 1 ,n_samples-1),...
                    'Color', color);
            end
        end
    end
    
    methods (Abstract)
        % Compute sample order for the plot.
        % All blocks need to have the samples in the same order. At
        % most 1 block can dictate a specific sample order for the plot.
        % Returns:
        % - sample_order: vector of indices or empty vector if the
        %   block does not dictate a specific sample order.
        GetSampleOrder(obj);
        
        % Compute maximum number of legend entries required for the block.
        % Returns:
        % - n_legend_entries: scalar, number of legend entries.
        GetNLegendEntries(obj);
        
        % Compute height (in mm) required for the block.
        % Returns:
        % - height: scalar, block height in mm.
        GetBlockHeight(obj, options);
        
        % Render block.
        % Input arguments:
        % - p: instance of panel class for the panel to be used for
        %   rendering the block;
        % - options: instance of PlotOptions class representing the plot
        %   options for HeatmapCovariatePlot;
        Plot(obj, p, options);
    end
end
