classdef Heatmap < HCP.Block
    properties
        % Cell array, name of columns from data table to include in heatmap;
        column_names;
        
        % Colormap, colormap to use for heatmap;
        cmap;
        
        % 2-element array [minimum, maximum] to map colors for colormap.
        % See also CLim;
        clim;
        
        % String, label for colorbar;
        title;
        
        % String, label for x axis (samples);
        xlabel;
        
        % String, label for y axis (features);
        ylabel;
        
        % Direction of y axis. See also YDir;
        ydir;
        
        % Logical, whether to standardize the data with mean=0 and std=1;
        standardize;
        
        % Logical, show sample dendrogram above heatmap;
        show_sample_dendrogram;
        
        % Logical, show feature dendrogram on the right of heatmap;
        show_feature_dendrogram;
        
        % Scalar, line width for lines in dendrogram(s);
        dendrogram_linewidth;
        
        % Sample tree (as return by linkage).
        % Node indices are based on original data order. See also linkage;
        sample_tree;
        
        % Feature tree (as return by linkage).
        % Node indices are based on original data order. See also linkage;
        feature_tree;
        
        % Scalar, number of clusters to use for sample clustering.
        % If set to 0, no cluster annotation is shown for samples;
        n_sample_clusters;
        
        % Scalar, number of clusters to use for feature clustering.
        % If set to 0, no cluster annotation is shown for features;
        n_feature_clusters;
        
        % Vector of sample indices indicating dictated sample order.
        % Empty array if no sample order is dictated by this heatmap;
        sample_order;
        
        % Vector of feature indices indicating dictated feature order.
        % Empty array if no feature order is dictated by this heatmap;
        feature_order;
        
        % Colormap, colormap to use for annotating sample clusters.
        % Only in use if n_sample_clusters>0;
        sample_cluster_cmap;
        
        % Colormap, colormap to use for annotating feature clusters.
        % Only in use if n_feature_clusters>0;
        feature_cluster_cmap;
        
        % Logical, show labels for samples on heatmap x axis;
        show_sample_labels;
        
        % Logical, show labels for features on heatmap y axis;
        show_feature_labels;
        
        % Scalar, height to use for each feature in heatmap, in mm.
        height_per_feature;
        
        % Scalar, space for sample labels, in mm.
        % Only in use if show_sample_labels is set;
        bottom_labels_space;
    end
    
    methods
        function obj = Heatmap(data, column_names, varargin)
            % Constructor for 'HCP.Heatmap'.
            % Input arguments:
            % - data: table with data to plot. Each row correspond to a feature
            %   (plotted as a row) and each column indicates a sample (plotted
            %   as column);
            % - column_names: cell array, name of columns from data table to
            %   include in heatmap;
            % - key/value pair:
            %   - cmap: colormap, colormap to use for heatmap. If brewermap
            %     is available, it defaults to RdBu (reversed), otherwise
            %     to jet;
            %   - sample_cluster_cmap: colormap, colormap to use for annotating
            %     sample clusters. Only in use if n_sample_clusters>0.
            %     If brewermap is available, it defaults to Set1, otherwise
            %     to hsv;
            %   - feature_cluster_cmap: colormap, colormap to use for annotating
            %     feature clusters. Only in use if n_feature_clusters>0.
            %     If brewermap is available, it defaults to Set1, otherwise
            %     to parula;
            %   - clim: 2-element array. Defaults to [0.5+minimum, 0.5+maximum]
            %     or [minimum, maximum] values for categorical
            %     and numeric columns, respectively;
            %   - title: string, label for colorbar. Defaults to no title;
            %   - xlabel: string, label for heatmap x axis (samples).
            %     Defaults to no label;
            %   - ylabel: string, label for heatmap y axis (features).
            %     Defaults to no label;
            %   - ydir: string, direction of y axis. Defaults to 'normal';
            %   - standardize: logical, if enabled standardize the data with
            %     mean=0 and std=1;. Defaults to false;
            %   - show_sample_dendrogram: logical, show sample dendrogram
            %     above heatmap. Defaults to true;
            %   - show_feature_dendrogram: logical, show feature dendrogram
            %     on the right of heatmap. Defaults to true;
            %   - dendrogram_linewidth: scalar, line width for lines
            %     in dendrogram(s). Defaults to 1;
            %   - n_sample_clusters: scalar, number of clusters to use for sample
            %     clustering. If set to 0, no cluster annotation is shown
            %     for samples;
            %   - n_feature_clusters: scalar, number of clusters to use for feature
            %     clustering. If set to 0, no cluster annotation is shown
            %     for features;
            %   - sample_tree: sample tree (as return by linkage). Node indices
            %     are based on original data order. Only in use if show_sample_dendrogram
            %     is set or n_sample_cluster>0. Defaults to using standard
            %     matlab linkage;
            %   - feature_tree: feature tree (as return by linkage). Node indices
            %     are based on original data order. Only in use if show_feature_dendrogram
            %     is set or n_feature_cluster>0. Defaults to using standard
            %     matlab linkage;
            %   - show_sample_labels: logical, show labels for samples on
            %     heatmap x axis. Defaults to true;
            %   - show_feature_labels: logical, show labels for features on
            %     heatmap y axis. Defaults to true;
            %   - linkage: string, linkage method to use when computing linkage
            %     for data. Defaults to 'average';
            %   - distance: string, distance metric to use when clustering
            %     data. Defaults to 'euclidean';
            %   - optimal_leaf_order: logical, use optimalleaforder() to
            %     determine leaf order in dendrograms. Default is to use the
            %     leaf order computed by dendrogram(). See also dendrogram and
            %     optimalleaforder;
            %   - height_per_feature: scalar, height to use for each feature
            %     in heatmap, in mm. Defaults to using same heigh as
            %     'covariate_row_height' in options;
            %   - bottom_labels_space: scalar, space for sample labels, in mm.
            %     Only in use if 'show_sample_labels' is enabled. Defaults
            %     to 10.
            
            ip = inputParser();
            if exist('brewermap', 'file')==2
                ip.addParameter('cmap', brewermap(256, '*RdBu'), @isnumeric);
                ip.addParameter('sample_cluster_cmap', brewermap(9, 'Set1'),...
                    @isnumeric);
                ip.addParameter('feature_cluster_cmap', brewermap(8, 'Set2'),...
                    @isnumeric);
            else
                ip.addParameter('cmap', jet(256), @isnumeric);
                ip.addParameter('sample_cluster_cmap', hsv(),...
                    @isnumeric);
                ip.addParameter('feature_cluster_cmap', parula(),...
                    @isnumeric);
            end
            ip.addParameter('clim', [], @isnumeric);
            ip.addParameter('title', '', @ischar);
            ip.addParameter('xlabel', '', @ischar);
            ip.addParameter('ylabel', '', @ischar);
            ip.addParameter('ydir', 'normal', @(x) ismember(x, {'normal', 'reverse'}));
            ip.addParameter('standardize', false, @islogical);
            ip.addParameter('show_sample_dendrogram', true, @islogical);
            ip.addParameter('show_feature_dendrogram', true, @islogical);
            ip.addParameter('dendrogram_linewidth', 1, @isnumeric);
            ip.addParameter('sample_tree', [], @isnumeric);
            ip.addParameter('feature_tree', [], @isnumeric);
            ip.addParameter('n_sample_clusters', 0, @isnumeric);
            ip.addParameter('n_feature_clusters', 0, @isnumeric);
            ip.addParameter('show_sample_labels', true, @islogical);
            ip.addParameter('show_feature_labels', true, @islogical);
            ip.addParameter('linkage', 'average');
            ip.addParameter('distance', 'euclidean');
            ip.addParameter('optimal_leaf_order', false, @islogical);
            ip.addParameter('height_per_feature', -1, @isnumeric);
            ip.addParameter('bottom_labels_space', 10, @isnumeric);
            ip.parse(varargin{:});
            
            if ip.Results.standardize
                data{:, column_names} = zscore(data{:, column_names});
            end
            
            obj = obj@HCP.Block(data(:, column_names));
            
            obj.column_names = column_names;
            obj.cmap = ip.Results.cmap;
            obj.clim = ip.Results.clim;
            obj.title = ip.Results.title;
            obj.xlabel = ip.Results.xlabel;
            obj.ylabel = ip.Results.ylabel;
            obj.ydir = ip.Results.ydir;
            obj.standardize = ip.Results.standardize;
            obj.show_sample_labels = ip.Results.show_sample_labels;
            obj.show_feature_labels = ip.Results.show_feature_labels;
            obj.dendrogram_linewidth = ip.Results.dendrogram_linewidth;
            obj.height_per_feature = ip.Results.height_per_feature;
            obj.bottom_labels_space = ip.Results.bottom_labels_space;
            
            obj.SetClusteringAttributes(obj.data{:, :}, 'sample', ip);
            obj.SetClusteringAttributes(obj.data{:, :}', 'feature', ip);
        end
        
        function sample_order = GetSampleOrder(obj)
            
            sample_order = obj.sample_order;
        end
        
        function n_legend_entries = GetNLegendEntries(~)
            
            n_legend_entries = 1;
        end
        
        function height = GetMainPanelRowHeight(obj, options)
            % Compute height of main panel for heatmap.
            % Main panel includes only core heatmap (imagesc) and excludes
            % top dendrogram, sample clustering annotation and sample
            % labels.
            % Input argument:
            % - options: instance of PlotOptions class;
            % Returns:
            % - height, height of main heatmap panel, in mm.
            
            if obj.height_per_feature==-1
                height = options.covariate_row_height * numel(obj.column_names);
            else
                height = obj.height_per_feature * numel(obj.column_names);
            end
        end
        
        function height = GetBlockHeight(obj, options)
            
            height = obj.GetMainPanelRowHeight(options);
            
            if obj.show_sample_dendrogram
                height = height + options.heatmap_dendrogram_size +...
                    options.heatmap_margin;
            end
            
            if obj.n_sample_clusters > 0
                height = height + options.covariate_row_height +...
                    options.heatmap_margin;
            end
            
            if obj.show_sample_labels
                height = height + obj.bottom_labels_space;
            end
        end
        
        function Plot(obj, p, options)
            
            obj.PlotTopDendrogram(p, options);
            
            p.pack('v', {{obj.GetMainPanelRowHeight(options)}});
            p_main_row = p(numel(p.children));
            [~, p_main, p_right] = obj.HorizontalBlockSplit(p_main_row, options);
            
            % Heatmap
            hax = p_main.select();
            p_main.fontsize = options.heatmap_labels_font_size;
            obj.PlotHeatmap(options);
            
            % Right metadata:
            obj.PlotRightDendrogram(p_right, options);
            p_right.pack('h', {{options.covariate_row_height}});
            p_colorbar = p_right(numel(p_right.children));
            obj.PlotColorBar(hax, p_colorbar, options);
            
            % Margins between elements:
            p.children.margintop = options.heatmap_margin;
            p.children.marginbottom = options.heatmap_margin;
            % Margin created with buffer panel in PlotRightDendrogram
            p_right.marginleft = 0;
            p_right.marginright = options.heatmap_margin;
        end
    end
    
    methods (Access = private)
        function SetClusteringAttributes(obj, data, dim, ip)
            % Parse arguments for clustering in one dimension and set
            % object attributes.
            % Input arguments:
            % - data: 2d matrix with data, re-arranged with the named
            %   dimension first;
            % - dim: string, name of the dimension to set clustering
            %   attributes for. Either 'sample' or 'feature';
            % - ip: instance of inputParser with input arguments parsed
            %   in the constructor.
            
            show_dendrogram = ip.Results.(['show_', dim, '_dendrogram']);
            n_clusters = ip.Results.(['n_', dim, '_clusters']);
            tree = ip.Results.([dim, '_tree']);
            
            if (show_dendrogram || n_clusters > 0)
                if isempty(tree)
                    dist = pdist(data, ip.Results.distance);
                    tree = linkage(dist, ip.Results.linkage);
                elseif ip.Results.optimal_leaf_order
                    error('Can not use optimal_leaf_order when specifying tree');
                end
                
                if ip.Results.optimal_leaf_order
                    order = optimalleaforder(tree, dist);
                else
                    f = figure();
                    [~, ~, order] = dendrogram(tree, size(data, 1));
                    close(f);
                end
            else
                % Keep order un-defined if no dendrogram is required.
                order = [];
            end
            
            obj.(['show_', dim, '_dendrogram']) = show_dendrogram;
            obj.(['n_', dim, '_clusters']) = n_clusters;
            obj.([dim, '_tree']) = tree;
            obj.([dim, '_order']) = order;
            obj.([dim, '_cluster_cmap']) = ip.Results.([dim, '_cluster_cmap']);
        end
        
        function PlotTopDendrogram(obj, p, options)
            % Plot sample dendrogram on top of main heatmap.
            % Input arguments:
            % - p: instance of panel class, panel to plot sample dendrogram in;
            % - options: instance of class PlotOptions.
            
            if obj.show_sample_dendrogram
                % Add row for dendrogram
                p.pack('v', {{options.heatmap_dendrogram_size}});
                [~, p_dendrogram] = obj.HorizontalBlockSplit(...
                    p(numel(p.children)), options);
                p_dendrogram.select();
                h = dendrogram(obj.sample_tree, height(obj.data),...
                    'Reorder', options.sample_order);
                set(h,...
                    'Color', 'k',...
                    'LineWidth', obj.dendrogram_linewidth);
                set(gca,...
                    'XLim', [0.5 height(obj.data)+0.5],...
                    'XTick', [],...
                    'YTick', [],...
                    'Color', 'none',...
                    'XColor', 'none',...
                    'YColor', 'none');
            end
            
            if obj.n_sample_clusters > 0
                % Add row for clusters annotation
                p.pack('v', {{options.covariate_row_height}});
                [~, p_clusters] = obj.HorizontalBlockSplit(...
                    p(numel(p.children)), options);
                p_clusters.select();
                clusters = cluster(obj.sample_tree,...
                    'MaxClust', obj.n_sample_clusters);
                clusters = clusters(obj.sample_order);
                imagesc(clusters', 'Tag', 'samples_clustering');
                set(gca,...
                    'XLim', [0.5 0.5+numel(clusters)],...
                    'XTick', [],...
                    'YTick', [],...
                    'XColor', 'k',...
                    'YColor', 'k',...
                    'Box', 'on',...
                    'Layer', 'top',...
                    'LineWidth', 0.5,...
                    'CLim', [0.5 0.5+max(clusters)]);
                obj.AddSampleLines(height(obj.data), 'k', options);
                colormap(gca, obj.sample_cluster_cmap);
            end
        end
        
        function PlotHeatmap(obj, options)
            % Plot main heatmap in current axis.
            % Input argument:
            % - options: instance of PlotOptions class.
            
            if isempty(obj.feature_order)
                ordered_data = obj.data(options.sample_order, :);
            else
                ordered_data = obj.data(options.sample_order, obj.feature_order);
            end
            
            imagesc(table2array(ordered_data)',...
                'AlphaData', ~isnan(table2array(ordered_data)'),...
                'Tag', strcat('heatmap_', obj.title));
            
            set(gca,...
                'XTick', 1:height(ordered_data),...
                'YTick', [1:width(ordered_data)],...
                'Box', 'on',...
                'Layer', 'top',...
                'XTickLabelRotation', 90,...
                'TickLength', [0, 0],...
                'XLim', [0.5 height(ordered_data)+0.5],...
                'YLim', [0.5 width(ordered_data)+0.5],...
                'YDir', obj.ydir,...
                'TickLabelInterpreter', 'none',...
                'FontName', options.font_name,...
                'XColor', 'k',...
                'YColor', 'k');
            
            if obj.show_sample_labels
                set(gca, 'XTickLabel', ordered_data.Properties.RowNames);
            else
                set(gca, 'XTickLabel', {});
            end
            
            if obj.show_feature_labels
                set(gca, 'YTickLabel', ordered_data.Properties.VariableNames);
            else
                set(gca, 'YTickLabel', {});
            end
            
            if ~isempty(obj.clim)
                set(gca, 'CLim', obj.clim);
            end
            
            obj.AddSampleLines(height(ordered_data), 'w', options);
            
            colormap(gca, obj.cmap);
            xlabel(obj.xlabel); %#ok<CPROP>
            ylabel(obj.ylabel); %#ok<CPROP>
        end
        
        function PlotRightDendrogram(obj, p, options)
            % Plot feature dendrogram on the right of main heatmap.
            % Input arguments:
            % - p: instance of panel class, panel to plot feature dendrogram in;
            % - options: instance of class PlotOptions.
            
            % Panel to create buffer between heatmap and cluster
            % annotation/dendrogram.
            p.pack('h', {{options.heatmap_margin}});
            p(1).marginright = 0;
            
            if obj.n_feature_clusters > 0
                p.pack('h', {{options.covariate_row_height}});
                p_clusters = p(2);
                clusters = cluster(obj.feature_tree,...
                    'MaxClust', obj.n_feature_clusters);
                clusters = clusters(obj.feature_order);
                p_clusters.select();
                imagesc(clusters,...
                    'Tag', 'features_clustering');
                set(gca,...
                    'YLim', [0.5 0.5+numel(clusters)],...
                    'XTick', [],...
                    'YTick', [],...
                    'YDir', obj.ydir,...
                    'XColor', 'k',...
                    'YColor', 'k',...
                    'Box', 'on',...
                    'Layer', 'top',...
                    'LineWidth', 0.5,...
                    'CLim', [0.5 0.5+max(clusters)]);
                colormap(gca, obj.feature_cluster_cmap);
            end
            
            % Plot feature dendrogram and cluster annotation
            if obj.show_feature_dendrogram
                p.pack('h', {{options.heatmap_dendrogram_size}});
                p_dendrogram = p(numel(p.children));
                p_dendrogram.select();
                h = dendrogram(obj.feature_tree, width(obj.data),...
                    'Reorder', obj.feature_order,...
                    'Orientation', 'right');
                set(h,...
                    'Color', 'k',...
                    'LineWidth', obj.dendrogram_linewidth);
                set(gca,...
                    'YLim', [0.5 width(obj.data)+0.5],...
                    'YDir', obj.ydir,...
                    'XTick', [],...
                    'YTick', [],...
                    'Color', 'none',...
                    'XColor', 'none',...
                    'YColor', 'none');
                p_dendrogram.marginright = 5;
            end
        end
        
        function PlotColorBar(obj, ax, p_colorbar, options)
            % Plot colorbar for main heatmap.
            % Input arguments:
            % - ax: handle to axis with the main heatmap the colorbar is for;
            % - p_colorbar: instance of panel class, panel to plot colorbar
            %   in;
            % - options: instance of class PlotOptions.
            
            cb = colorbar(ax);
            cb.Label.String = obj.title;
            cb.Label.FontName = options.font_name;
            cb.Label.FontSize = options.font_size;
            cb.XColor = 'k';
            cb.YColor = 'k';
            p_colorbar.select(cb);
        end
    end
end
