---
title: 'HCP: A Matlab package to create beautiful heatmaps with richly annotated covariates'

tags:
  - MATLAB
  - HeatmapCovariatePlot
  - annotated heatmap
  - data visualization
  - data exploration
  - multivariate plotting
authors:
 - name: Manuela Salvucci
   orcid: 0000-0001-9941-4307
   affiliation: 1
 - name: Jochen H. M. Prehn
   orcid: 0000-0003-3479-7794
   affiliation: 1
affiliations:
 - name: Centre for Systems Medicine, Department of Physiology and Medical Physics, Royal College of Surgeons in Ireland, Dublin, Ireland
   index: 1
date: 20 January 2019
bibliography: paper.bib
---

# Summary
A heatmap is a graphical technique that maps 2-dimensional matrices of numerical values to colors to provide an immediate
and intuitive visualization of the underlying patterns [@Eisen1998]. Heatmaps are often used in conjunction with cluster
analysis to re-order observations and/or features by similarity and thus, rendering common and distinct patterns more apparent.
When generating these visualizations, it is often of interest to interpret the underlying patterns in the context of other
data sources. In the field of bioinformatics, heatmaps are frequently used to visualize high-throughput and high-dimensional
datasets, such as those derived from profiling biological samples with *-omic* technologies (whole genome sequencing,
transcriptomics and proteomics). Often, biological samples (for example, patient tumour samples) are characterized at
multiple *-omic* level and it is of interest to contrast and compare patterns captured at the different molecular layers
along with their associations with other observable features (covariates). The concurrent display of continuous or
categorical covariates enriches the visualization with additional information such as group membership.

The R-language features powerful packages to create annotated heatmaps with complex layouts such as *pheatmap* [@Kolde2019],
*ComplexHeatmap* [@Gu2016] and *Superheat* [@Barter2018]. However, to the best of our knowledge, Matlab only features the
*HeatMap* and *Clustergram* classes (from the *Bioinformatics Toolbox*) to draw simple heatmaps with/without cluster analysis
with basic annotations and there is no package with the breath of those in the R ecosystem. The HCP package fills this gap
by enabling bioinformaticians and data scientists from other fields to create richly annotated visualizations for data 
exploration in Matlab.

*HCP* (*HeatmapCovariatePlot*) provides a simple high level application programming interface (API) to design elaborated visualizations in a modular
fashion. The user can select which elements to include, covariate row annotations and/or heatmaps, by invoking the `AddCovariateRow`
or the `AddHeatmap` methods. Elements can be vertically stacked and also grouped in functionally related sub-blocks
encapsulated by the `AddSubBlock` method to adjust the figure layout. The plotting options in *HCP* are chosen sensibly
to create production-quality out-of-the-box visualizations in most use-case. *HCP* features several plotting options to
adjust the plot aesthetics to cater for the user preferences in terms of colormaps, labelling, legends and layouts (margins
and positions). *HCP* ease-of-use and rapidity enables the users to iterate through multiple visualization alternatives
while focusing on the message conveyed by the data rather than the technicalities involved in generating the plot.

*HCP* plotting functionality has been applied in a scholarly manuscript currently undergoing revisions and in exploratory
analyses in several other ongoing research projects.

[Figure 1](#markdown-header-case-study-1-clinical-and-molecular-characterization-of-a-cohort-of-cutaneous-melanoma-patients) and
[Figure 2](#markdown-header-case-study-2-demographic-socio-economic-and-work-occupations-by-us-state) showcase visualizations generated with *HCP* for a case study drawn from a bioinformatic application in
cancer research (cutaneous melanoma) and a more general data science application (occupation by US state).

## Case study 1: Clinical and molecular characterization of a cohort of cutaneous melanoma patients

![tcga_cutaneous_melanoma_cancer.png](https://bitbucket.org/manuela_s/hcp/raw/master/+examples/figures/tcga_cutaneous_melanoma_cancer.png)
**Figure 1.** *Case study 1.* Integrated visualization of clinico-pathological features [@Liu2018] and molecular alterations in
mutational status, copy number alterations (CNAs) [@Akbani2015], immune cell composition [@Thorsson2018] and protein profiles [@TCGANetwork; @Grossman2016] determined in primary tumour samples of n=61 cutaneous melanoma patients from the [The Cancer Genome Atlas network (TCGA)](https://cancergenome.nih.gov/) cohort.
Source data for the figure are further detailed in [tcga_cutaneous_melanoma_cancer.md](https://bitbucket.org/manuela_s/hcp/src/master/+examples/data/tcga_cutaneous_melanoma_cancer.md).

## Case study 2: Geographical, demographic, socio-economic and work occupations by US state

![occupation_by_us_state.png](https://bitbucket.org/manuela_s/hcp/raw/master/+examples/figures/occupation_by_us_state.png)
**Figure 2.** *Case study 2.* Integrated visualization of geographical [@RCoreTeam; @USDepartmentofCommerce1977; @USDepartmentofCommerce1977a],
demographic and socio-economic metadata [@TheKaiserFamilyFoundationsStateHealthFacts]
and work occupations by US state [@BureauofLaborStatistics]. Source data for the figure are further detailed in [occupation_by_us_state.md](https://bitbucket.org/manuela_s/hcp/src/master/+examples/data/occupation_by_us_state.md).

# Acknowledgements
*HCP* uses extensively the *panel* package [@Mitch]. ColorBrewer colormaps [@CynthiaABrewer] are supported *via* the *brewermap*
package [@Cobeldick].

The visualization for case study 1 shown here is based upon data generated by the TCGA Research Network: http://cancergenome.nih.gov/.
The visualization for case study 2 is based largely upon data generated by the Henry J Kaiser Family Foundation: https://www.kff.org/
and by the Bureau of Labor Statistics: https://www.bls.gov/oes/.

This package was developed to support research funded by Science Foundation Ireland (13/IA/1881 and 14/IA/2582).

# References