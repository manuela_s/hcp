% HCP
% 
% HCP (HeatmapCovariatePlot) is an open-source Matlab package to generate
% publication-quality, highly customizable heatmaps annotated with
% continuous or categorical metadata variables.
%
% Workflow:
% 0. Prepare data as a Matlab table, one row per sample (will be visualized
%    as a column) and one column per feature (will be visualized as a row).
% 1. Instantiate HeatmapCovariatePlot object with the data table and
%    additional options to customize the plot appearance.
% 2. Add required block(s) to the plot. Any of the following blocks, in any
%    order and/or combinations are supported:
%    * feature block: Add blocks with metadata features to the plot by
%      calling AddCovariateRow on the block property of the
%      HeatmapCovariatePlot instance with a column name from the data
%      table. Categorical or numerical columns can be used as metadata
%      features. Additional arguments can be passed to customize the
%      appearance of individual metadata blocks. Multiple variables can
%      also be grouped together in sub-blocks.
%    * heatmap block: Add heatmaps to the plot by calling AddHeatmap on the
%      block property of the HeatmapCovariatePlot instance with a
%      cell-array with the column names for the data to include in the
%      heatmap. Additional arguments can be passed to customize the
%      appearance.
% 3. Call the plot method on the HeatmapCovariatePlot instance to plot all
%    the blocks that were added in a Matlab figure.
% 4. The HeatmapCovariatePlot can also be exported directly to pdf, png...
%    with the Export method.
% 
% Files
%   Block                - Building block for HeatmapCovariatePlot.
%   BlockContainer       - Container for a group of blocks for 'HeatmapCovariatePlot'.
%   CovariateRow         - Block for representing a covariate in the HeatmapCovariatePlot.
%   Heatmap              - A false color 2D image of the data values in a matrix.
%   HeatmapCovariatePlot - Class to create heatmap plot with multiple covariates.
%   PlotOptions          - Plotting options that collectively apply to the blocks included in
%
% See also HCP.HeatmapCovariatePlot, HCP.BlockContainer.AddCovariateRow,
% HCP.BlockContainer.AddHeatmap, HCP.HeatmapCovariatePlot.plot and
% HeatmapCovariatePlot.Export.
