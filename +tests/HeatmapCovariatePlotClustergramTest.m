classdef HeatmapCovariatePlotClustergramTest < matlab.unittest.TestCase
    % Unittests for HeatmapCovariatePlot to check behaviour against Matlab
    % clustergram.
    % Run with runtests('HCP.tests.HeatmapCovariatePlotClustergramTest')
    
    properties (Constant)
        % Table with test datasets
        data = HCP.examples.get.OccupationByUSState();
        
        % Column-names for occupation columns in data
        occupations = HCP.tests.HeatmapCovariatePlotTest.data.Properties.VariableNames(20:end);
    end
    
    properties
        % Figure handle for the figure used by the test;
        fig;
        % Name for temporary file;
        tmp_name;
    end
    
    methods (TestMethodSetup)
        function MethodSetup(testCase)
            testCase.fig = figure();
            testCase.tmp_name = [tempname(), '.pdf'];
        end
    end
    
    methods (TestMethodTeardown)
        function MethodTeardown(testCase)
            close(testCase.fig);
            % Close any clustergrams created by the test:
            close(findall(0, 'Type', 'Figure', 'Tag', 'Clustergram'));
            if exist(testCase.tmp_name, 'file')
                delete(testCase.tmp_name);
            end
        end
    end
    
    methods (Test)        
        function TestMatchesClustergram(testCase)
            % Test that heatmap matches clustergram results.
            % Check that heatmap with default dendrogram matches behavior
            % of clustergram from matlab bioinformatics toolbox.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap(testCase.occupations);
            p.Plot();
            
            gco = clustergram(testCase.data{:, testCase.occupations}',...
                'RowLabels', testCase.occupations,...
                'ColumnLabels', testCase.data.Properties.RowNames,...
                'DisplayRange', max(max(testCase.data{:, testCase.occupations}')),...
                'Symmetric', false,...
                'OptimalLeafOrder', false); %#ok<UDIM>
            
            heatmap_image = findobj(gcf, 'type', 'Image');
            
            testCase.verifyEqual(heatmap_image.Parent.XTickLabel',...
                gco.ColumnLabels);
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                gco.RowLabels);
        end
        
        function TestMatchesClustergramOptimalLeafOrder(testCase)
            % Test that heatmap matches clustergram results with OptimalLeafOrder enabled.
            % Check that heatmap with default dendrogram matches behavior
            % of clustergram from matlab bioinformatics toolbox when
            % optimal_leaf_order is enabled.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap(testCase.occupations,...
                'optimal_leaf_order', true);
            p.Plot();
            
            gco = clustergram(testCase.data{:, testCase.occupations}',...
                'RowLabels', testCase.occupations,...
                'ColumnLabels', testCase.data.Properties.RowNames,...
                'DisplayRange', max(max(testCase.data{:, testCase.occupations}')),...
                'Symmetric', false); %#ok<UDIM>
            
            heatmap_image = findobj(gcf, 'type', 'Image');
            
            testCase.verifyEqual(heatmap_image.Parent.XTickLabel',...
                gco.ColumnLabels);
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                gco.RowLabels);
        end
        
        function TestMatchesClustergramLinkageDistance(testCase)
            % Test that heatmap matches clustergram results with linkage and distance flags.
            % Check that heatmap with default dendrogram matches behavior
            % of clustergram from matlab bioinformatics toolbox when
            % using a non-default linkage and distance metrics.
            
            linkage = 'complete';
            distance = 'cityblock';
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap(testCase.occupations,...
                'linkage', linkage,...
                'distance', distance);
            p.Plot();
            
            gco = clustergram(testCase.data{:, testCase.occupations}',...
                'RowPDist', distance,...
                'ColumnPDist', distance,...
                'linkage', linkage,...
                'RowLabels', testCase.occupations,...
                'ColumnLabels', testCase.data.Properties.RowNames,...
                'DisplayRange', max(max(testCase.data{:, testCase.occupations}')),...
                'Symmetric', false,...
                'OptimalLeafOrder', false); %#ok<UDIM>
            
            heatmap_image = findobj(gcf, 'type', 'Image');
            testCase.verifyEqual(heatmap_image.Parent.XTickLabel',...
                gco.ColumnLabels);
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                gco.RowLabels);
        end
        
        function TestMatchesClustergramFeatureStandardization(testCase)
            % Test that heatmap matches clustergram results with standardization.
            % Check that heatmap with feature standardization (z-scores)
            % matches behavior of clustergram from matlab bioinformatics
            % toolbox.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap(testCase.occupations,...
                'standardize', true);
            p.Plot();
            
            clustergram(testCase.data{:, testCase.occupations}',...
                'Standardize', 'row',...
                'RowLabels', testCase.occupations,...
                'ColumnLabels', testCase.data.Properties.RowNames,...
                'DisplayRange', max(max(testCase.data{:, testCase.occupations})),...
                'Symmetric', false,...
                'OptimalLeafOrder', false);
            
            % Get handle for heatmap imagesc from clustergram figure
            heatmap_image = findobj(...
                findall(0, 'Type', 'Figure', 'Tag', 'Clustergram'),...
                'type', 'Image');
            testCase.verifyEqual(...
                p.blocks.sub_blocks{1}.data{p.blocks.sub_blocks{1}.sample_order,...
                p.blocks.sub_blocks{1}.feature_order},...
                heatmap_image.CData');
        end
        
        function TestCustomTree(testCase)
            % Test HeatmapCovariatePlot specifying sample_tree and feature_tree.
            
            sample_Z = linkage(testCase.data{:, testCase.occupations}, 'single', 'cityblock');
            feature_Z = linkage(testCase.data{:, testCase.occupations}', 'single', 'cityblock');
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations,...
                'sample_tree', sample_Z,...
                'feature_tree', feature_Z,...
                'title', 'occupations');
            p.Plot();
            
            % Test that row and column order matches clustergram when using
            % same distance and linkage as was used for the custom trees:
            gco = clustergram(testCase.data{:, testCase.occupations}',...
                'RowLabels', testCase.occupations,...
                'ColumnLabels', testCase.data.Properties.RowNames,...
                'OptimalLeafOrder', false,...
                'RowPDist', 'cityblock',...
                'ColumnPDist', 'cityblock',...
                'Linkage', 'single');
            
            heatmap_image = findobj(gcf, 'type', 'Image', 'Tag', 'heatmap_occupations');
            testCase.verifyEqual(heatmap_image.Parent.XTickLabel',...
                gco.ColumnLabels);
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                gco.RowLabels);
        end
    end
end
