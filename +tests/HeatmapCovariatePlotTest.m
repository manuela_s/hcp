classdef HeatmapCovariatePlotTest < matlab.unittest.TestCase
    % Unittests for HeatmapCovariatePlot and helper classes.
    % Run with runtests('HCP.tests.HeatmapCovariatePlotTest')
    
    properties (Constant)
        % Table with test datasets
        data = HCP.examples.get.OccupationByUSState();
        
        % Column-names for occupation columns in data
        occupations = HCP.tests.HeatmapCovariatePlotTest.data.Properties.VariableNames(20:end);
    end
    
    properties
        % Figure handle for the figure used by the test;
        fig;
        % Name for temporary file;
        tmp_name;
    end
    
    methods (TestMethodSetup)
        function MethodSetup(testCase)
            testCase.fig = figure();
            testCase.tmp_name = [tempname(), '.pdf'];
        end
    end
    
    methods (TestMethodTeardown)
        function MethodTeardown(testCase)
            close(testCase.fig);
            if exist(testCase.tmp_name, 'file')
                delete(testCase.tmp_name);
            end
        end
    end
    
    methods (Test)
        function TestSimplePlot(testCase)
            % Test that HeatmapCovariatePlot does not error when making plot.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region'); % Categorical
            p.blocks.AddCovariateRow('total_residents_millions'); % Numeric
            p.blocks.AddHeatmap(testCase.occupations);
            p.Plot();
        end
        
        function TestSubBlock(testCase)
            % Test that HeatmapCovariatePlot does not error when making subblocks.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            sb = p.blocks.AddSubBlock('title', 'Meta');
            sb.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations);
            p.Plot();
        end
        
        function TestCovariateRowPlot(testCase)
            % Test that plotting CovariateRow does not error.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('total_residents_millions',...
                'cmap', hot(),...
                'clim', [0 50],...
                'title', 'Total residents',...
                'unit', '[mil]');
            p.Plot();
        end
        
        function TestHeatmapPlot(testCase)
            % Check that appearance flags can be set for Heatmap without error.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap(testCase.occupations,...
                'cmap', hot(),...
                'clim', [0 0.1],...
                'title', 'Occupations data for US states',...
                'xlabel', 'US states',...
                'ylabel', 'Occupations',...
                'ydir', 'reverse',...
                'dendrogram_linewidth', 2,...
                'sample_cluster_cmap', jet(),...
                'feature_cluster_cmap', jet(),...
                'show_sample_labels', false,...
                'show_feature_labels', false,...
                'height_per_feature', 3);
            p.Plot();
        end
        
        function TestMultiHeatmap1(testCase)
            % Test adding multiple heatmaps without enabling sample dendrograms.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap({'median_household_income_k', 'per_capita_state_spending_k'},...
                'show_sample_dendrogram', false);
            p.blocks.AddHeatmap(testCase.occupations,...
                'show_sample_dendrogram', false);
            p.Plot();
        end
        
        function TestMultiHeatmap2(testCase)
            % Test adding multiple heatmaps where only one heatmap has 'show_sample_dendrogram' enabled.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap({'median_household_income_k', 'per_capita_state_spending_k'},...
                'show_sample_dendrogram', true);
            p.blocks.AddHeatmap(testCase.occupations,...
                'show_sample_dendrogram', false);
            p.Plot();
        end
        
        function TestMultiHeatmap3(testCase)
            % Test adding multiple heatmaps where 'show_sample_dendrogram' is enabled.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap({'median_household_income_k', 'per_capita_state_spending_k'},...
                'show_sample_dendrogram', true);
            p.blocks.AddHeatmap(testCase.occupations,...
                'show_sample_dendrogram', true);
            
            % HeatmapCovariatePlot need one consisten sample order for the
            % whole plot. Plot should fail if trying to include multiple
            % heatmaps with sample dendrograms.
            testCase.verifyError(@p.Plot,...
                'HCP:GetSampleOrder:OnlyOneSampleOrderCanBeSpecified');
        end
        
        function TestOrderingWithNoDendrograms(testCase)
            % Check that ordering of samples and features is correct without dendrograms.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations,...
                'show_sample_dendrogram', false,...
                'show_feature_dendrogram', false,...
                'title', 'occupations');
            p.Plot();
            covariate_image = findobj(gcf,...
                'type', 'Image',...
                'tag', 'covariate_row_state_region');
            heatmap_image = findobj(gcf,...
                'type', 'Image',...
                'tag', 'heatmap_occupations');
            
            % Labels and data should match original order if no
            % dendrograms were included.
            testCase.verifyEqual(covariate_image.CData,...
                grp2idx(testCase.data.state_region)');
            testCase.verifyEqual(heatmap_image.CData,...
                testCase.data{:, testCase.occupations}');
            testCase.verifyEqual(heatmap_image.Parent.XTickLabel,...
                testCase.data.Properties.RowNames);
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                testCase.occupations');
        end
        
        function TestOrderingWithFeatureDendrogram(testCase)
            % Test feature ordering with feature dendrogram.
            % Check that feature order is consistent between heatmap data
            % and labels when 'show_feature_dendrogram' is enabled.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddHeatmap(testCase.occupations,...
                'show_sample_dendrogram', false,...
                'show_feature_dendrogram', true,...
                'title', 'occupations');
            p.Plot();
            
            heatmap_image = findobj(gcf,...
                'type', 'Image',...
                'tag', 'heatmap_occupations');
            features_order = p.blocks.sub_blocks{1}.feature_order;
            testCase.verifyEqual(heatmap_image.CData,...
                testCase.data{:, testCase.occupations(features_order)}');
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                testCase.occupations(features_order)');
        end
        
        function TestOrderingWithSampleDendrogram(testCase)
            % Test sample ordering with sample dendrogram.
            % Check that ordering of samples is applied consistently to
            % covariates, heatmaps and labels when 'has_sample_dendrogram'
            % is enabled.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations,...
                'show_sample_dendrogram', true,...
                'show_feature_dendrogram', false,...
                'title', 'occupations');
            p.Plot();
            covariate_image = findobj(gcf,...
                'type', 'Image',...
                'tag', 'covariate_row_state_region');
            heatmap_image = findobj(gcf,...
                'type', 'Image',...
                'tag', 'heatmap_occupations');
            % Get order that samples were reordered in:
            [~, idx] = ismember(heatmap_image.Parent.XTickLabel,...
                testCase.data.Properties.RowNames);
            % Verify that data was reordered the same way:
            testCase.verifyEqual(covariate_image.CData,...
                grp2idx(testCase.data.state_region(idx))');
            testCase.verifyEqual(heatmap_image.CData,...
                testCase.data{idx, testCase.occupations}');
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                testCase.occupations');
        end
        
        function TestOrderingWithSampleAndFeatureDendrogram(testCase)
            % Test sample and feature ordering with dendrograms enabled.
            % Check that ordering of samples and features is applied
            % consistently when both 'show_sample_dendrogram' and
            % 'show_feature_dendrogram' are enabled.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations,...
                'show_sample_dendrogram', true,...
                'show_feature_dendrogram', true,...
                'title', 'occupations');
            p.Plot();
            covariate_image = findobj(gcf,...
                'type', 'Image',...
                'tag', 'covariate_row_state_region');
            heatmap_image = findobj(gcf,...
                'type', 'Image',...
                'tag', 'heatmap_occupations');
            features_order = p.blocks.sub_blocks{2}.feature_order;
            
            % Get order that samples were reordered in:
            [~, idx] = ismember(heatmap_image.Parent.XTickLabel,...
                testCase.data.Properties.RowNames);
            % Verify that data was reordered the same way:
            testCase.verifyEqual(covariate_image.CData,...
                grp2idx(testCase.data.state_region(idx))');
            testCase.verifyEqual(heatmap_image.CData,...
                testCase.data{idx, testCase.occupations(features_order)}');
            testCase.verifyEqual(heatmap_image.Parent.YTickLabel,...
                testCase.occupations(features_order)');
        end
        
        function TestClusteringAnnotationFlags(testCase)
            % Test that flags to annotate clusters work correctly.
            
            n_sample_clusters = 2;
            n_feature_clusters = 3;
            sample_cluster_cmap = [1, 0, 0; 0, 0, 1;];
            feature_cluster_cmap = [1, 1, 0; 0, 1, 0; 0.5, 0.5, 0.5;];
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations,...
                'n_sample_clusters', n_sample_clusters,...
                'n_feature_clusters', n_feature_clusters,...
                'sample_cluster_cmap', sample_cluster_cmap,...
                'feature_cluster_cmap', feature_cluster_cmap);
            p.Plot();
            
            samples_clustering = findobj(gcf,...
                'type', 'Image',...
                'tag', 'samples_clustering');
            features_clustering = findobj(gcf,...
                'type', 'Image',...
                'tag', 'features_clustering');
            testCase.verifyEqual(numel(unique(samples_clustering.CData)),...
                n_sample_clusters);
            testCase.verifyEqual(numel(unique(features_clustering.CData)),...
                n_feature_clusters);
            testCase.verifyEqual(colormap(samples_clustering.Parent),...
                sample_cluster_cmap);
            testCase.verifyEqual(colormap(features_clustering.Parent),...
                feature_cluster_cmap);
            
        end
        
        function TestPlotCustomPanel(testCase)
            % Test that HeatmapCovariatePlot does not error when plotting in custom panel.
            
            pa = panel();
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations);
            p.Plot('parent_panel', pa);
            
            testCase.verifyNotEmpty(pa.children);
        end
        
        function TestExport(testCase)
            % Test that HeatmapCovariatePlot does not error when exporting.
            
            p = HCP.HeatmapCovariatePlot(testCase.data);
            p.blocks.AddCovariateRow('state_region');
            p.blocks.AddHeatmap(testCase.occupations);
            p.Export(testCase.tmp_name);
            
            testCase.verifyEqual(exist(testCase.tmp_name, 'file'), 2);
        end
    end
end