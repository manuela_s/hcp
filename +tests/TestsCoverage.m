% Run test suite and generate coverage report

coverage = matlab.unittest.plugins.CodeCoveragePlugin.forPackage('HCP');
suite = [matlab.unittest.TestSuite.fromClass(?HCP.tests.HeatmapCovariatePlotTest),...
    matlab.unittest.TestSuite.fromClass(?HCP.tests.HeatmapCovariatePlotClustergramTest)];
runner = matlab.unittest.TestRunner.withTextOutput();
runner.addPlugin(coverage);
runner.run(suite)
