## Sources for occupations_by_us_state.csv

### General state features (columns 1-5) 
extracted from 'state' dataset from the package 'datasets' in R.

Data sources:

  - R Core Team (2017). R: A language and environment for statistical computing. Foundation for Statistical 
    Computing, Vienna, Austria. URL https://www.R-project.org/;
  - U.S. Department of Commerce, Bureau of the Census (1977) Statistical Abstract of the United States;
  - U.S. Department of Commerce, Bureau of the Census (1977) County and City Data Book;
 
### State Health Facts (columns 6-19)
   
derived from [The Kaiser Family Foundationís State Health Facts [access date 2019-01-11]](https://www.kff.org/).

Data sources:

  - [Kaiser Family Foundation estimates based on the Census Bureau's American Community Survey, 2008-2017.](https://www.kff.org/other/state-indicator/total-residents/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [Kaiser Family Foundation estimates based on the Census Bureau's American Community Survey, 2008-2017.](https://www.kff.org/other/state-indicator/distribution-by-gender/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [Kaiser Family Foundation estimates based on the Census Bureau's American Community Survey, 2008-2017.](https://www.kff.org/other/state-indicator/distribution-by-age/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [Kaiser Family Foundation estimates based on the Census Bureau's American Community Survey, 2008-2017.](https://www.kff.org/other/state-indicator/distribution-by-family-structure/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [Kaiser Family Foundation estimates based on the Census Bureau's American Community Survey, 2008-2017.](https://www.kff.org/other/state-indicator/distribution-by-citizenship-status/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [U.S. Census Bureau, Current Population Survey, November 2014 and 2016; Voting and Registration Tables.](https://www.kff.org/other/state-indicator/number-of-voters-and-voter-registration-in-thousands-as-a-share-of-the-voter-population/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [U.S. Department of Justice, Office of Justice Programs, Bureau of Justice Statistics, Correctional Populations in the United States Series, Accessed June, 28, 2018.](https://www.kff.org/other/state-indicator/total-population-in-u-s-adult-correctional-systems-by-correctional-status/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [U.S. Department of Housing and Urban Development, Point in Time Estimates of Homelessness, 2017, December 2017.](https://www.kff.org/other/state-indicator/estimates-of-homelessness/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [U.S. Census Bureau, 2017 Current Population Survey, Annual Social and Economic Supplements Data Tables. Historical Household Income.](https://www.kff.org/other/state-indicator/median-annual-income/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [KCMU calculations based on adjusted data collected in the National Association of State Budget Officers (NASBO) State Expenditure Report: Examining Fiscal 2015-2017 State Spending, November 2017; and the U.S. Census Bureau Resident Population Data, 2016.](https://www.kff.org/other/state-indicator/per-capita-state-spending/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [KFF calculation based on data from the U.S. Census Bureau, 2017 Annual Survey of State Government Tax Collections and the U.S. Census Bureau Population Estimates, accessed April 10, 2018.](https://www.kff.org/other/state-indicator/state-collections-per-capita/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [U.S. Bureau of Economic Analysis (BEA), Broad Growth Across States in 2014: Advance Statistics of GDP by State, Table 4. Current-Dollar GDP by State, 2011-2014, June 10, 2015.](https://www.kff.org/other/state-indicator/total-gross-state-product/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D);
  - [Bureau of Labor Statistics (BLS), Regional and State Employment and Unemployment (Monthly), Civilian labor force and unemployment by state and selected area, seasonally adjusted, September report for 2013-2018.](https://www.kff.org/other/state-indicator/unemployment-rate/?currentTimeframe=0&sortModel=%7B%22colId%22:%22Location%22,%22sort%22:%22asc%22%7D).
  
### Occupations (columns 20-41)
derived from [Bureau of Labor Statistics, U.S. Department of Labor, Occupational Employment Statistics, (https://www.bls.gov/oes/, accessed 2019-01-03).](https://www.bls.gov/oes/special.requests/oesm17st.zip)