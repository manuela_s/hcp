function t = TCGACutaneousMelanomaCancer()
% Get example dataset for a clinical cancer application for cutaneous melanoma.
% Returns:
% - t: MATLAB table with one row per patient and one column per feature.
%      See also tcga_cutaneous_melanoma_cancer.md.

t = readtable(fullfile('+HCP', '+examples', 'data',...
    'tcga_cutaneous_melanoma_cancer.csv'));

cat_vars = {'stage', 'sex', 'death', 'mutation_subtypes',...
    'uv_signature', 'pigment_score', 'ulceration'};
for i = 1:numel(cat_vars)
    t.(cat_vars{i}) = categorical(t.(cat_vars{i}));
end

mutations = {'BRAF_mut', 'KRAS_mut', 'NRAS_mut', 'HRAS_mut', 'NF1_mut'};
for m = 1:numel(mutations)
    t.(mutations{m}) = categorical(t.(mutations{m}));
    t.(mutations{m}) = reordercats(t.(mutations{m}), {'wild-type', 'mutant'});
end

cnas = {'BRAF_cna', 'KRAS_cna', 'NRAS_cna', 'HRAS_cna', 'NF1_cna'};
for c = 1:numel(cnas)
    t.(cnas{c}) = categorical(t.(cnas{c}), {'-', '0', '+'});
    t.(cnas{c}) = reordercats(t.(cnas{c}), {'-', '0', '+'});
end

t.sex = reordercats(t.sex, {'M', 'F'});
t.death = reordercats(t.death, {'yes', 'no'});
t.ulceration = reordercats(t.ulceration, {'yes', 'no'});

t.Properties.RowNames = t.bcr_patient_barcode;
end
