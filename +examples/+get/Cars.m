function t = Cars()
% Get MATLAB 'carsmall.mat' dataset (from the Statistics Toolbox) and export
% as MATLAB table with data prepared for plot.
% Output:
% - t: MATLAB table with one row per car sample and columns:
%      'Model', 'Origin', 'MPG', 'Cylinders', 'Displacement'
%      'Horsepower', 'Weight', 'Acceleration', 'Model_Year' and 'Mfg'.
% See also https://uk.mathworks.com/help/stats/sample-data-sets.html.

t = struct2table(load('carsmall.mat'));

% Convert variables to categorical
t.Origin = categorical(cellstr(t.Origin));
t.Origin = reordercats(...
    mergecats(t.Origin, {'France', 'Germany', 'Italy', 'Sweden'}, 'Europe'),...
    {'Europe', 'USA', 'Japan'});
t.Cylinders = categorical(t.Cylinders);

% Drop samples with missing values in the numeric features of interest
num_vars = {'MPG', 'Displacement', 'Horsepower', 'Weight', 'Acceleration'};
t(any(isnan(t{:, num_vars}), 2), :) = [];

t = sortrows(t, {'Model_Year', 'Origin', 'Cylinders'}, 'ascend');
end
