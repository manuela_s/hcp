function t = OccupationByUSState()
% Get datasets with statistics by US state regarding occupations and corresponding metadata by state and export
% as MATLAB table with data prepared for plot.
% Output:
% - t: MATLAB table with one row per US state and one column per feature.
%      See also occupation_by_us_state.md.

t = readtable(fullfile('+HCP', '+examples', 'data',...
    'occupation_by_us_state.csv'));

cat_vars = {'state_region', 'state_division', 'state_name', 'state_abb'};
for c=1:numel(cat_vars)
    t.(cat_vars{c}) = categorical(t.(cat_vars{c}));
end

occupations = t.Properties.VariableNames(20:end);

% Compute percent of residents with occupation per state
t{:, occupations} = t{:, occupations} ./...
    repmat(t.total_residents, 1, numel(occupations)) .* 100;

% Compute as percent of residents per state
t.community_supervision_percent = t.community_supervision ./ t.total_residents .* 100;
t.incarcerated_percent = t.incarcerated ./ t.total_residents .* 100;
t.people_experiencing_homelessness_percent = t.total_people_experiencing_homelessness ./...
    t.total_residents .* 100;

% Express total residents in millions
t.total_residents_millions = t.total_residents/1e6;

% Express financial features in thousands
t.median_household_income_k = t.median_household_income/1e3;
t.per_capita_state_spending_k = t.per_capita_state_spending/1e3;
t.per_capita_state_collections_k = t.per_capita_state_collections/1e3;
t.per_capita_gross_state_product_k = (1e6*t.total_gross_state_product ./ t.total_residents)/1e3;

% Include variables of interest
t = t(:, [{'state_region', 'state_division', 'state_name', 'state_abb',...
    'frost', 'total_residents_millions', 'male_to_female_ratio',...
    'adults_to_children_ratio', 'adults_with_children_to_adults_with_no_children_ratio',...
    'citizen_to_non_citizen_ratio', 'registered_to_actually_voted_ratio',...
    'community_supervision_percent', 'incarcerated_percent',...
    'people_experiencing_homelessness_percent', 'median_household_income_k',...
    'per_capita_state_spending_k', 'per_capita_state_collections_k',...
    'per_capita_gross_state_product_k', 'unemployment_rate_seasonally_adjusted'},...
    occupations]);

t.Properties.RowNames = cellstr(t.state_abb);
end
