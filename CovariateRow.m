classdef CovariateRow < HCP.Block
    % Block for representing a covariate in the HeatmapCovariatePlot.
    % The covariate row renders the data from categorical or numeric column
    % from the data table as a horizontal row.
    
    properties (Access = private)
        % String, name of the column from the data table to plot;
        column_name;
        
        % Colormap, colormap to use for the current covariate;
        cmap;
        
        % 2-element array [minimum, maximum] to map colors for colormap.
        % See also CLim.
        clim;
        
        % String, title for covariate row rendered on the left side of the block;
        title;
        
        % String, unit for block rendered on the right side of the block;
        unit;
    end
    
    methods
        function obj = CovariateRow(data, column_name, varargin)
            % Constructor for 'HCP.CovariateRow'.
            % Input argument:
            % - data: MATLAB table with data to plot;
            % - column_name: name of column in the data table to plot;
            % - key/value pairs:
            %   - cmap: colormap for the covariate. If brewermap is
            %     available, defaults to brewer 'Set1' and 'Greens" for
            %     categorical and numeric columns, respectively. Otherwise,
            %     it defaults to 'hsv' and 'summer' for categorical and
            %     numeric columns, respectively.
            %   - clim: 2-element array. Defaults to [0.5+minimum,
            %     0.5+maximum] or [minimum, maximum] values for categorical
            %     and numeric columns, respectively;
            %   - title: string, title for the covariate. Defaults to using
            %     the column name;
            %   - unit: string, unit for the covariate. Defaults to no unit.
            
            ip = inputParser();
            ip.addParameter('cmap', [], @isnumeric);
            ip.addParameter('clim', [], @isnumeric);
            ip.addParameter('title', column_name, @ischar);
            ip.addParameter('unit', '', @ischar);
            ip.parse(varargin{:});
            
            obj = obj@HCP.Block(data.(column_name));
            obj.column_name = column_name;
            obj.cmap = ip.Results.cmap;
            obj.clim = ip.Results.clim;
            obj.title = ip.Results.title;
            obj.unit = ip.Results.unit;
        end
        
        function sample_order = GetSampleOrder(~)
            
            sample_order = [];
        end
        
        function n_legend_entries = GetNLegendEntries(obj)
            
            if iscategorical(obj.data)
                n_legend_entries = numel(categories(obj.data));
            else
                n_legend_entries = 1;
            end
        end
        
        function height = GetBlockHeight(~, options)
            
            height = options.covariate_row_height;
        end
        
        function Plot(obj, p, options)
            
            [~, p_main, p_right] = obj.HorizontalBlockSplit(p, options);
            p_main.select();
            obj.PlotData(options);
            obj.PlotLegend(p_right, p_main.object, options);
        end
    end
    
    methods (Access = private)
        function cmap = GetCMap(obj)
            % Helper function to get colormap for the covariate.
            % Returns:
            % - cmap: colormap for the covariate.
            
            if ~isempty(obj.cmap)
                cmap = obj.cmap;
            elseif iscategorical(obj.data)
                if exist('brewermap', 'file')==2
                    cmap = brewermap(numel(categories(obj.data)), 'Set1');
                else
                    cmap = hsv(numel(categories(obj.data)));
                end
            else
                if exist('brewermap', 'file')==2
                    cmap = brewermap(1024, 'Greens');
                else
                    cmap = summer(1024);
                end
                % Skip first 10% of colormap, to make undefined values (white)
                % seperatable
                cmap = cmap(100:end, :);
            end
        end
        
        function clim = GetCLim(obj)
            % Helper function to get limits for colormap for the covariate.
            % Returns:
            % - clim: 2-element array (cmin, cmax) to map data to the colormap.
            
            if ~isempty(obj.clim)
                clim = obj.clim;
            elseif iscategorical(obj.data)
                clim = [0.5 numel(categories(obj.data))+0.5];
            else
                clim = [min(obj.data), max(obj.data)];
            end
        end
        
        function PlotData(obj, options)
            % Helper function to plot the data for the covariate.
            % Input argument:
            % - options: instance of PlotOptions class representing the plot
            %   options for HeatmapCovariatePlot.
            
            if iscategorical(obj.data)
                values = grp2idx(obj.data(options.sample_order));
                alpha = ~isundefined(obj.data(options.sample_order));
            else
                values = obj.data(options.sample_order);
                alpha = ~isnan(obj.data(options.sample_order));
            end
            
            imagesc(values',...
                'AlphaData', alpha',...
                'Tag', strcat('covariate_row_', obj.column_name));
            ylabel(obj.title,...
                'Rotation', 0,...
                'Interpreter', 'none',...
                'HorizontalAlignment', 'right',...
                'VerticalAlignment', 'middle');
            set(gca,...
                'XLim', [0.5 0.5+numel(values)],...
                'XTick', [],...
                'YTick', [],...
                'XColor', 'k',...
                'YColor', 'k',...
                'Box', 'on',...
                'Layer', 'top',...
                'LineWidth', 0.5,...
                'CLim', obj.GetCLim());
            obj.AddSampleLines(numel(obj.data), 'k', options);
            colormap(gca, obj.GetCMap());
        end
        
        function PlotLegend(obj, legend_p, data_ax, options)
            % Helper function to add legend for covariate.
            % Input arguments:
            % - legend_p: instance of panel class, panel to add legend in;
            % - data_ax: axis handle, handle to axis where data is plotted;
            % - options: instance of PlotOptions class representing the plot
            %   options for HeatmapCovariatePlot.
            
            % Split panel in 3 for legend:
            % - buffer, with space for colorbar min label (continuous);
            % - legends (categorical) / colorbar (continuous);
            % - buffer, for space for colorbar max label (continuous) /
            %   last legend entry.
            % Cannot use margin on the left, or it would affect sample
            % data width.
            legend_p.pack('h',...
                {{options.covariate_legend_intraspace},...
                [],...
                {options.covariate_legend_entry_width}});
            legend_p.children.margin=0;
            
            if iscategorical(obj.data)
                cats = categories(obj.data);
                % Make one panel for each category
                legend_p(2).pack('h',...
                    repmat({{options.covariate_row_height}}, 1, numel(cats)));
                legend_p(2).children.marginright = options.covariate_legend_entry_width;
                for c=1:numel(cats)
                    legend_p(2,c).select();
                    imagesc(c);
                    colormap(gca, colormap(data_ax));
                    set(gca,...
                        'CLim', get(data_ax, 'CLim'),...
                        'XTick', [],...
                        'YTick', [],...
                        'XColor', 'k',...
                        'YColor', 'k',...
                        'Box', 'on',...
                        'Layer', 'top',...
                        'LineWidth', 0.5);
                    text(...
                        (options.covariate_row_height + options.covariate_legend_text_margin)/10,...
                        options.covariate_row_height/2/10,...
                        cats{c},...
                        'Units', 'centimeters',... % Convert to mm
                        'FontName', options.font_name,...
                        'FontSize', options.font_size,...
                        'Interpreter', 'none');
                end
            else
                cb = colorbar(data_ax,...
                    'Location', 'north');
                legend_p(2).select(cb);
                set(cb,...
                    'XTick', [],...
                    'YTick', [],...
                    'XColor', 'k',...
                    'YColor', 'k',...
                    'Box', 'on',...
                    'LineWidth', 0.5);
                
                % Undocumented DecorationContainer to get axes-like handle:
                cb_ax = cb.DecorationContainer;
                
                legend_p(1).select();
                legend_p(3).select();
                set([legend_p(1).object,legend_p(3).object],...
                    'XTick', [],...
                    'YTick', [],...
                    'XColor', 'none',...
                    'YColor', 'none',...
                    'Color', 'none',...
                    'Box', 'off');
                colorbar_width = options.legend_block_width -...
                    options.covariate_legend_intraspace -...
                    options.covariate_legend_entry_width;
                text(-options.covariate_legend_text_margin/10,...
                    options.covariate_row_height/2/10,...
                    num2str(cb.XLim(1)),...
                    'Units', 'centimeters',...
                    'HorizontalAlignment', 'right',...
                    'FontName', options.font_name,...
                    'FontSize', options.font_size,...
                    'Parent', cb_ax);
                text((colorbar_width + options.covariate_legend_text_margin)/10,...
                    options.covariate_row_height/2/10,...
                    sprintf('%s %s', num2str(cb.XLim(2)), obj.unit),...
                    'Units', 'centimeters',...
                    'FontName', options.font_name,...
                    'FontSize', options.font_size,...
                    'Parent', cb_ax);
            end
        end
    end
end
