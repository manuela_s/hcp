[![MATLAB](https://img.shields.io/badge/MATLAB-tested%20on%20MATLAB%202014b-red.svg)](MATLAB) [![License](https://img.shields.io/badge/License-BSD%202--Clause-blue.svg)](LICENSE) [![JOSS](http://joss.theoj.org/papers/10.21105/joss.01291/status.svg)](https://doi.org/10.21105/joss.01291) [![ZENODO](https://zenodo.org/badge/DOI/10.5281/zenodo.3242593.svg)](http://doi.org/10.5281/zenodo.3242593)

# Table of contents
[TOC]

# What is HCP?
*HCP* (*HeatmapCovariatePlot*) is an open-source Matlab package to generate publication-quality, highly customizable heatmaps annotated with continuous or categorical metadata variables.

![Cars example](+examples/figures/cars.png "Cars example")

## Why HCP?
Pairing heatmaps with custom annotations provides a powerful visualization to highlight patterns in multi-dimensional data and their associations with metadata. Annotated heatmaps are widely adopted in data science in general and bioinformatics in particular. Native Matlab only features the HeatMap and Clustergram classes (from the Bioinformatics Toolbox) to draw simple heatmaps with/without cluster analysis with basic annotations and to the best of our knowledge no 3rd party package is available to create annotated heatmaps with complex layouts such as those available in the R ecosystem (for example, [pheatmap](https://cran.r-project.org/web/packages/pheatmap/index.html), [ComplexHeatmap](https://www.ncbi.nlm.nih.gov/pubmed/27207943) and [Superheat](https://www.ncbi.nlm.nih.gov/pubmed/30911216)). The *HCP* package fills this gap by enabling bioinformaticians and data scientists from other fields to create richly annotated visualizations for data exploration in Matlab.

# Installation

## Prerequisites
*HCP* has been developed and tested in *Matlab 2014b* and requires the Statistics Toolbox. A subset of the unit tests also use the Bioinformatics toolbox.

The package makes extensive use of Matlab tables (introduced in R2013b) and thus it is not compatible with older Matlab versions.

*HCP* has been tested in Ubuntu (18.04) and Windows (10).

*HCP* requires the following open source Matlab packages to be installed:

- *panel*: download from [Matlab File Exchange](https://uk.mathworks.com/matlabcentral/fileexchange/20003-panel);
- *brewermap*: download from [Matlab File Exchange](https://uk.mathworks.com/matlabcentral/fileexchange/45208-colorbrewer-attractive-and-distinctive-colormaps).
  Brewermap is used for default colormaps in *HCP*, if installed. If brewermap is not available, built-in Matlab colormaps are used instead.

The packages must be installed and on the Matlab search path.
See [Matlab documentation](https://uk.mathworks.com/help/matlab/matlab_env/what-is-the-matlab-search-path.html).

## HCP installation
1. Download [newest released version](https://bitbucket.org/manuela_s/HCP/downloads/?tab=tags);
2. Unzip to a [Matlab package folder](https://uk.mathworks.com/help/matlab/matlab_oop/scoping-classes-with-packages.html) named `+HCP`.

Alternatively *HCP* can be installed with git.

1. `git clone git@bitbucket.org:manuela_s/HCP.git +HCP` (SSH) or
   `git clone https://manuela_s@bitbucket.org/manuela_s/HCP.git +HCP` (HTTPS)

The top level *+HCP* package directory must be on the Matlab search path.
See [Matlab documentation](https://uk.mathworks.com/help/matlab/matlab_env/what-is-the-matlab-search-path.html).

# Usage

## Getting started
The following code is used to create the annotated heatmap in [What is HCP](#markdown-header-what-is-hcp):

- Prepare data as a Matlab table, one row per sample (will be visualized as a column) and one column per feature (will be visualized as a row). 
```Matlab
     % Load data
     data = HCP.examples.get.Cars();
```

- Instantiate HeatmapCovariatePlot object with the data table and additional options to customize the plot appearance.
```Matlab
     % Initialize HeatmapCovariatePlot
     p = HCP.HeatmapCovariatePlot(data,...
       'left_labels_space', 20);
```

- Add blocks with metadata features to the plot by calling *AddCovariateRow* on the *block* property of the *HeatmapCovariatePlot* instance with a column name from the data table. Categorical or numerical columns can be used as metadata   features. Additional arguments can be passed to customize the appearance of individual metadata blocks. Multiple variables can also be grouped together in sub-blocks.
See [Reference documentation](#markdown-header-reference-documentation) for details.
```Matlab
     % Add metadata features
     p.blocks.AddCovariateRow('Model_Year',...
       'cmap', [198, 219, 239; 66, 146, 198; 8, 81, 156]./255,...
       'title', 'Model year');
     p.blocks.AddCovariateRow('Origin');
     p.blocks.AddCovariateRow('Cylinders',...
       'cmap', brewermap(numel(data.Cylinders), 'Reds'),...
       'unit', '[#]');
```

- Add heatmaps to the plot by calling *AddHeatmap* on the *block* property of the *HeatmapCovariatePlot* instance with a cell-array with the column names for the data to include in the heatmap. Additional arguments can be passed to customize the appearance.
```Matlab
     % Add heatmap with clustering for selected car features
     p.blocks.AddHeatmap({'MPG', 'Displacement', 'Horsepower', 'Weight', 'Acceleration'},...
       'height_per_feature', 2,...
       'standardize', true,...
       'show_sample_dendrogram', false,...
       'show_feature_dendrogram', true,...
       'n_feature_clusters', 2,...
       'feature_cluster_cmap', brewermap(2, 'Greys'),...
       'show_sample_labels', false,...
       'title', 'Z-scores');
```
  
- Call the *plot* method on the *HeatmapCovariatePlot* instance to plot all the blocks that were added in a Matlab figure.
```Matlab
     % Plot
     p.Plot();
```

- The *HeatmapCovariatePlot* can also be exported directly to pdf, png... with the *Export* method.
```Matlab
     % Export figure as pdf
     p.Export(fullfile('+HCP', '+examples', 'figures', 'cars.pdf'),...
       '-w180',...
       '-rx');
```

Full code for the car example is available in [Cars.m](+examples/Cars.m) with resulting figure in [cars.png](+examples/figures/cars.png).

### Additional examples
See the following more in-depth examples using additional *HCP* features.

#### TCGA Cutaneous Melanoma Cancer ([code](+examples/TCGACutaneousMelanomaCancer.m))
![tcga_cutaneous_melanoma_cancer.png](+examples/figures/tcga_cutaneous_melanoma_cancer.png)

#### Occupation By US State ([code](+examples/OccupationByUSState.m))
![occupation_by_us_state.png](+examples/figures/occupation_by_us_state.png)

## Reference documentation
To access the full reference documentation, see:
```Matlab
doc HCP
```

# Contributing
Feature requests, enhancements and bug fixes are welcome.
Please follow the [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md).

## Reporting issues
Any issues or feature requests can be added to the BitBucket [issue tracker](https://bitbucket.org/manuela_s/hcp/issues?status=new&status=open).

When reporting issues, please include:

- description of the problem and minimal example;
- stack-trace / error-message;
- Matlab version and OS.

## Contributing code
To contribute fixes, features or enhancements, please submit a
[pull request](https://bitbucket.org/manuela_s/hcp/pull-requests/) *via* BitBucket.
Please include a description of the change. Please check that all tests pass and include new tests to cover the change.

## Testing
*HCP*'s core functionality can be tested by running:

```Matlab
runtests('HCP.tests.HeatmapCovariatePlotTest')
% Note, the following tests set requires Bioinformatics toolbox:
runtests('HCP.tests.HeatmapCovariatePlotClustergramTest') 
```

# Credits
This package makes use of the [panel package by Ben Mitch](https://uk.mathworks.com/matlabcentral/fileexchange/20003-panel). The beautiful default [Brewer colormaps](http://www.personal.psu.edu/cab38/ColorBrewer/ColorBrewer_updates.html) are from the [brewermap package by Stephen Cobeldick](https://uk.mathworks.com/matlabcentral/fileexchange/45208-colorbrewer-attractive-and-distinctive-colormaps).

*HCP* was inspired by existing packages to make complex, publication-quality annotated heatmaps available in R including [pheatmap](https://github.com/raivokolde/pheatmap), [superheat](https://github.com/rlbarter/superheat) and [ComplexHeatmap](https://github.com/jokergoo/ComplexHeatmap).

The visualization for the [TCGA Cutaneous Melanoma Cancer example](#markdown-header-TCGA Cutaneous Melanoma Cancer) shown here is based upon data generated by the TCGA Research Network: http://cancergenome.nih.gov/. The visualization for the [Occupation By US State example](#markdown-header-Occupation By US State) is based largely upon data generated by the Henry J Kaiser Family Foundation: https://www.kff.org/ and by the Bureau of Labor Statistics: https://www.bls.gov/oes/. Data sources are detailed in [tcga_cutaneous_melanoma_cancer.md](../+examples/data/tcga_cutaneous_melanoma_cancer.md) and [occupation_by_us_state.md](../+examples/data/occupation_by_us_state.md).

This package was developed to support research funded by Science Foundation Ireland (13/IA/1881 and 14/IA/2582).

# Licence
*HCP* is released under the [2-Clause BSD License](LICENSE).

# Citing *HCP*
If you use *HCP* in your work, please cite the following paper and Zenodo archive:

- Salvucci et al., (2019). HCP: A Matlab package to create beautiful heatmaps with richly annotated covariates. Journal of Open Source Software, 4(38), 1291, [https://doi.org/10.21105/joss.01291](https://doi.org/10.21105/joss.01291);
- Manuela Salvucci, & Jochen H.M. Prehn. (2019, June 10). HCP: A Matlab package to create beautiful heatmaps with richly annotated covariates (Version 1.0). Zenodo. [http://doi.org/10.5281/zenodo.3242593](http://doi.org/10.5281/zenodo.3242593).